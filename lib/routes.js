FlowRouter.route('/', {
  subscriptions: function (params, queryParams) {
    //this.register('posts', Meteor.subscribe('allPosts'));
    this.register('userData', Meteor.subscribe('userData'));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: 'front' });
  },
  name: 'home'
});
FlowRouter.route('/sign/up', {
  subscriptions: function (params, queryParams) {
    //this.register('posts', Meteor.subscribe('allPosts'));
    //this.register('featured', Meteor.subscribe('featuredBusinesses'));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: 'signup' });
  },
  name: 'signup'
});
FlowRouter.route('/sign/in', {
  subscriptions: function (params, queryParams) {
    //this.register('posts', Meteor.subscribe('allPosts'));
    //this.register('featured', Meteor.subscribe('featuredBusinesses'));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: 'signin' });
  },
  name: 'signin'
});
FlowRouter.route('/profile', {
  subscriptions: function (params, queryParams) {
    this.register('userData', Meteor.subscribe('userData'));
    this.register('images', Meteor.subscribe('images'));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: 'profile' });
  },
  name: 'profile'
});
FlowRouter.route('/publish', {
  subscriptions: function (params, queryParams) {
    this.register('userData', Meteor.subscribe('userData'));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: 'publish' });
  },
  name: 'publish'
});
FlowRouter.route('/404', {
  subscriptions: function (params, queryParams) {
    this.register('userData', Meteor.subscribe('userData'));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: '404' });
  },
  name: '404'
});
FlowRouter.route('/:username', {
  subscriptions: function (params, queryParams) {
    this.register('userData', Meteor.subscribe('userData', params.username));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: 'micropage' });
  },
  name: 'micropage'
});
FlowRouter.route('/:username/:slug', {
  subscriptions: function (params, queryParams) {
    this.register('userData', Meteor.subscribe('userData', params.username));
    this.register('userContent', Meteor.subscribe('userContent', params.username, params.slug));
  },
  action: function (params, queryParams) {
    FlowLayout.render('layout', {main: 'microcontent' });
  },
  name: 'microcontent'
});
