
AutoForm.hooks({
  NewPostForm: {
    onSuccess: function (formType, result) {
      this.event.preventDefault();
      FlowRouter.go('/'+Meteor.user().username);
      return false;
    },
  },
  UserProfileEdit: {
    onSuccess: function (formType, result) {
      this.event.preventDefault();
      $("html, body").animate({ scrollTop: 0 }, "slow");
      return false;
    },
  }
});
