Template.micropage.onCreated(function () {
  this.autorun(function() {
    if (FlowRouter.subsReady()) {
      var user = Meteor.users.findOne({username: FlowRouter.getParam('username')});
      if (!user) {
         FlowRouter.go('/404');
      }
    }
  });
})
