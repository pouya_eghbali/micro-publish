Template.microcontent.onCreated(function () {
  this.autorun(function() {
    if (FlowRouter.subsReady()) {
      var post = Posts.findOne({'slug': FlowRouter.getParam('slug')});
      if (!post) {
         FlowRouter.go('/404');
      }
    }
  });
})
Template.microcontent_contents.helpers({
  visitorIsCreator: function () {
    return Meteor.user() && Meteor.user().username == FlowRouter.getParam('username');
  },
  fixOEmbeds: function () {
    // Fix Script loads from oembeds:

    if($('.twitter-tweet')) {
      $.getScript ( '//platform.twitter.com/widgets.js', function(){
        $('.twitter-tweet').each(function() {
          var tid = $(this).attr('data-tweet-id');
          var id = $(this).attr('id');
          var div = $('<div></div>');
          div.attr('id', id);
          $(this).replaceWith(div);
          twttr.widgets.createTweetEmbed(tid, document.getElementById(id));
        })
      });
    }
    
  }
})

Template.microcontent_contents.events({
  'click .remove': function () {
    Posts.remove(this._id, function (error) {
      if(!error){
        FlowRouter.go('/'+Meteor.user().username)
      }
    });
  }
});
