/*
	Strata by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
*/

(function($) {

	var settings = {

		// Parallax background effect?
			parallax: true,

		// Parallax factor (lower = more intense, higher = less intense).
			parallaxFactor: 20

	};

	skel.breakpoints({
		xlarge: '(max-width: 1800px)',
		large: '(max-width: 1280px)',
		medium: '(max-width: 980px)',
		small: '(max-width: 736px)',
		xsmall: '(max-width: 480px)'
	});

	// go to top:

	$('body').prepend('<a href="#" class="back-to-top"><i class="fa fa-arrow-circle-o-up"></i></a>');
	$('body').prepend('<a href="/" class="home-btn"><i class="fa fa-home"></i></a>');
	var amountScrolled = 300;

	$(window).scroll(function() {
		if ( $(window).scrollTop() > amountScrolled ) {
			$('a.back-to-top').fadeIn('slow');
		} else {
			$('a.back-to-top').fadeOut('slow');
		}
	});

	$('a.back-to-top').click(function() {
		$('body, html').animate({
			scrollTop: 0
		}, 700);
		return false;
	});

	$(document).ready(function () {
		if($('.editor').html() == '') {
			$('.editor').html(
				'<h2> عنوان نمونه (ویرایش کنید) </h2><p> متن خود را اینجا بنویسید! </p>'
			)
		}
		var _medium_editor = new MediumEditor('.editor', {
			placeholder: {text: 'متن خود را وارد کنید.'},
			anchor: {placeholderText: 'لینک را وارد کنید.',
							 targetCheckboxText: 'در یک پنجره جدید باز شود'}
		});
		/*_medium_editor.subscribe('editableInput', function (event, editable) {
				var s = _medium_editor.serialize(); s = s[Object.keys(s)[0]].value;
				$('textarea.editor').text(s);
		});*/
		$(function () {
		    $('.editor').mediumInsert({
		        editor: _medium_editor,
						addons: {
							images: { // (object) Image addon configuration
		            label: '<span class="fa fa-camera"></span>', // (string) A label for an image addon
		            captions: true, // (boolean) Enable captions
		            captionPlaceholder: 'عنوان را تایپ کنید (اختیاری)', // (string) Caption placeholder
		            autoGrid: 3, // (integer) Min number of images that automatically form a grid
		        },
							embeds: { // (object) Embeds addon configuration
		            label: '<span class="fa fa-code"></span>', // (string) A label for an embeds addon
		            placeholder: 'یک آدرس از یوتیوب، ویمئو، فیسبوک، توئیتر، اینستاگرام، فلیکر، دویان‌آرت، گیست یا هر جای دیگر وارد کنید!', // (string) Placeholder displayed when entering URL to embed
		            captions: true, // (boolean) Enable captions
		            captionPlaceholder: 'عنوان را تایپ کنید (اختیاری)', // (string) Caption placeholder
		            oembedProxy: 'http://open.iframe.ly/api/oembed?iframe=0', // (string/null) URL to oEmbed proxy endpoint, such as Iframely, Embedly or your own. You are welcome to use "http://medium.iframe.ly/api/oembed?iframe=1" for your dev and testing needs, courtesy of Iframely. *Null* will make the plugin use pre-defined set of embed rules without making server calls.
		        	}
						}
		    });
		});
	})

	$(document).ready(function () {
		$(".js-remove").html('<i class="fa fa-times"></i> حذف');
	})

	$(document).ready(function(){
		if (typeof $('.tab-container')[0] != 'undefined') {
			$('ul.tabs li').click(function(){
				var tab_id = $(this).attr('data-tab');
				$('ul.tabs li').removeClass('current');
				$('.tab-content').removeClass('current');
				$(this).addClass('current');
				$("#"+tab_id).addClass('current');
				window.dispatchEvent(new Event('resize'));
			})
		}
	})

	$(function() {

		var $window = $(window),
			$body = $('body'),
			$header = $('#header');

		// Disable animations/transitions until the page has loaded.
			$body.addClass('is-loading');

			$window.on('load', function() {
				$body.removeClass('is-loading');
			});

		// Touch?
			if (skel.vars.mobile) {

				// Turn on touch mode.
					$body.addClass('is-touch');

				// Height fix (mostly for iOS).
					window.setTimeout(function() {
						$window.scrollTop($window.scrollTop() + 1);
					}, 0);

			}

		// Fix: Placeholder polyfill.
			$('form').placeholder();

		// Prioritize "important" elements on medium.
			skel.on('+medium -medium', function() {
				$.prioritize(
					'.important\\28 medium\\29',
					skel.breakpoint('medium').active
				);
			});

		// Header.

			// Parallax background.

				// Disable parallax on IE (smooth scrolling is jerky), and on mobile platforms (= better performance).
					if (skel.vars.browser == 'ie'
					||	skel.vars.mobile)
						settings.parallax = false;

				if (settings.parallax) {

					skel.on('change', function() {

						if (skel.breakpoint('medium').active) {

							$window.off('scroll.strata_parallax');
							$header.css('background-position', 'top left, center center');

						}
						else {

							$header.css('background-position', 'left 0px');

							/*$window.on('scroll.strata_parallax', function() {
								$header.css('background-position', 'left ' + (-1 * (parseInt($window.scrollTop()) / settings.parallaxFactor)) + 'px');
							});*/

						}

					});

				}

	});

})(jQuery);
